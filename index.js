var API = require('./lib');

API.setup();

API.start(function (info) {
	console.log('EmberSC API started on port: ', info.port);
});
