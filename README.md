# Ember-SC API

Ember SC Node.js API

## Dependencies
* [Node.js](http://nodejs.org) or [NVM](https://github.com/creationix/nvm)

## Installation

* `git clone https://bitbucket.org/corbinu/embersc-api` this repository
* `cd embersc-api`
* `npm install`

## Running / Development

* `node .`
* Visit your API at http://localhost:3000.
* To reset the test data go to http://localhost:3000/api/v1/reset

### Running Tests

* `npm test`


### Deploying

Deploy as a service with [pm2](https://github.com/Unitech/pm2) 
Behind a proxy like Nginx

