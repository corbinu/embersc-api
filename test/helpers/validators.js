var Lab = require('lab');

var validators = {};

validators.speakers = {
    bugs: function(speaker) {
        Lab.expect(speaker).to.have.property('id', '1');
        Lab.expect(speaker).to.have.property('name', 'Bugs Bunny');
        Lab.expect(speaker).to.have.property('presentations').to.be.an('array').length(2).to.include.members([ '1', '2' ]);
    },
    wile: function(speaker) {
        Lab.expect(speaker).to.have.property('id', '2');
        Lab.expect(speaker).to.have.property('name', 'Wile E. Coyote');
        Lab.expect(speaker).to.have.property('presentations').to.be.an('array').length(1).to.include.members([ '3' ]);
    },
    yosemite: function(speaker) {
        Lab.expect(speaker).to.have.property('id', '3');
        Lab.expect(speaker).to.have.property('name', 'Yosemite Sam');
        Lab.expect(speaker).to.have.property('presentations').to.be.an('array').length(3).to.include.members([ '4', '5', '6' ]);
    }
};

validators.presentations = {
    whatsUp: function(presentation) {
        Lab.expect(presentation).to.have.property('id', '1');
        Lab.expect(presentation).to.have.property('title', 'What\'s up with Docs?');
        Lab.expect(presentation).to.have.property('speaker', '1');
    },
    ofCourse: function(presentation) {
        Lab.expect(presentation).to.have.property('id', '2');
        Lab.expect(presentation).to.have.property('title', 'Of course, you know, this means war.');
        Lab.expect(presentation).to.have.property('speaker', '1');
    },
    gettingTheMost: function(presentation) {
        Lab.expect(presentation).to.have.property('id', '3');
        Lab.expect(presentation).to.have.property('title', 'Getting the most from the Acme catalog.');
        Lab.expect(presentation).to.have.property('speaker', '2');
    },
    shaaaadUp: function(presentation) {
        Lab.expect(presentation).to.have.property('id', '4');
        Lab.expect(presentation).to.have.property('title', 'Shaaaad up!');
        Lab.expect(presentation).to.have.property('speaker', '3');
    },
    ahHates: function(presentation) {
        Lab.expect(presentation).to.have.property('id', '5');
        Lab.expect(presentation).to.have.property('title', 'Ah hates rabbits.');
        Lab.expect(presentation).to.have.property('speaker', '3');
    },
    theGreat: function(presentation) {
        Lab.expect(presentation).to.have.property('id', '6');
        Lab.expect(presentation).to.have.property('title', 'The Great horni-todes');
        Lab.expect(presentation).to.have.property('speaker', '3');
    }
};

module.exports = validators;