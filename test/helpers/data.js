var data = {};

data.speakers = [
    {
        id: '1',
        name: 'Bugs Bunny',
        presentations: [ '1', '2' ]
    },
    {
        id: '2',
        name: 'Wile E. Coyote',
        presentations: [ '3' ]
    },
    {
        id: '3',
        name: 'Yosemite Sam',
        presentations: [ '4', '5', '6' ]
    }
];

data.presentations = [
    {
        id: '1',
        title: "What's up with Docs?",
        speaker: "1"
    },
    {
        id: '2',
        title: "Of course, you know, this means war.",
        speaker: "1"
    },
    {
        id: '3',
        title: "Getting the most from the Acme catalog.",
        speaker: "2"
    },
    {
        id: '4',
        title: "Shaaaad up!",
        speaker: "3"
    },
    {
        id: '5',
        title: "Ah hates rabbits.",
        speaker: "3"
    },
    {
        id: '6',
        title: "The Great horni-todes",
        speaker: "3"
    }
];

module.exports = data;