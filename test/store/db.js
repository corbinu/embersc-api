var Lab = require('lab');

var API = require('../../lib');
var db = require('../../lib/store/db');

var lab = exports.lab = Lab.script();

lab.experiment('DB', function () {

    lab.test('basic', function (done) {

        Lab.expect(db).to.have.property('setup').that.is.an('function');
        Lab.expect(db).to.have.property('mrstore');

        done();
    });

    lab.test('setup', function (done) {

        db.setup(API.config);

        done();
    });

});