var Lab = require('lab');

var API = require('../../lib');
var store = require('../../lib/store');

var lab = exports.lab = Lab.script();

lab.experiment('Store', function () {

    lab.test('basic', function (done) {

        Lab.expect(store).to.have.property('db');
        Lab.expect(store).to.have.property('models');
        Lab.expect(store).to.have.property('setup').that.is.an('function');
        Lab.expect(store).to.have.property('start').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        store.setup(API.config);

        done();
    });

    lab.test('start', function (done) {

        store.start(done);
    });

});