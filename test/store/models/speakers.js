var Lab = require('lab');

var API = require('../../../lib');
var speakers = require('../../../lib/store/models/speakers');

var lab = exports.lab = Lab.script();

lab.experiment('Speakers', function () {

    lab.test('basic', function (done) {

        Lab.expect(speakers).to.have.property('reset').that.is.an('function');
        Lab.expect(speakers).to.have.property('setup').that.is.an('function');
        Lab.expect(speakers).to.have.property('start').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        speakers.setup(API.config, API.store.db);

        done();
    });

    lab.test('reset', function (done) {

        speakers.reset(done);
    });


});