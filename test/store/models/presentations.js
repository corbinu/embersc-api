var Lab = require('lab');

var API = require('../../../lib');
var presentations = require('../../../lib/store/models/presentations');

var lab = exports.lab = Lab.script();

lab.experiment('Presentations', function () {

    lab.test('basic', function (done) {

        Lab.expect(presentations).to.have.property('setup').that.is.an('function');
        Lab.expect(presentations).to.have.property('start').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        presentations.setup(API.config, API.store.db);

        done();
    });

    lab.test('reset', function (done) {

        presentations.reset(done);
    });


});