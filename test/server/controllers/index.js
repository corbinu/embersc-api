var Lab = require('lab');

var API = require('../../../lib');
var controllers = require('../../../lib/server/controllers');

var lab = exports.lab = Lab.script();

lab.experiment('Controllers', function () {

    lab.test('basic', function (done) {

        Lab.expect(controllers).to.have.property('base');
        Lab.expect(controllers).to.have.property('reset');
        Lab.expect(controllers).to.have.property('speakers');
        Lab.expect(controllers).to.have.property('setup').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        controllers.setup(API.config, API.store);

        done();
    });

});