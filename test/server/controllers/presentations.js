var Lab = require('lab');

var API = require('../../../lib');
var helpers  = require('../../helpers');
var presentationsController = require('../../../lib/server/controllers/presentations');

var lab = exports.lab = Lab.script();

lab.experiment('Presentations', function () {

    lab.test('basic', function (done) {

        Lab.expect(presentationsController).to.have.property('setup').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        presentationsController.setup(API.config, API.store);

        done();
    });

    lab.experiment('POST', function() {

        lab.before(function (done) {

            API.setup();
            API.store.start(function (err) {
                if (err) return done(err);

                API.store.models.presentations.removeAll(done);
            });
        });

        lab.test('add', function (done) {
            var options = {
                method: "POST",
                url: API.config.base + '/presentations',
                payload: { presentation: helpers.data.presentations[0] }
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('presentation');
                helpers.validators.presentations.whatsUp(result.presentation);

                done();
            });
        });

        lab.test('add multiple', function (done) {
            var presentations = helpers.data.presentations.slice(1);

            var options = {
                method: "POST",
                url: API.config.base + '/presentations',
                payload: { presentations: presentations }
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('presentations');

                helpers.verifyArray(result.presentations, 'id', {
                    2: helpers.validators.presentations.ofCourse,
                    3: helpers.validators.presentations.gettingTheMost,
                    4: helpers.validators.presentations.shaaaadUp,
                    5: helpers.validators.presentations.ahHates,
                    6: helpers.validators.presentations.theGreat
                });

                done();
            });
        });

        lab.test('add again', function (done) {

            var options = {
                method: "POST",
                url: API.config.base + '/presentations',
                payload: { presentations: helpers.data.presentations[0] }
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(409);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 409);
                Lab.expect(result).to.have.property('error', 'Conflict');
                Lab.expect(result).to.have.property('message', 'Error: Presentation with id 1 already exists');

                done();
            });
        });
    });

    lab.experiment('GET', function() {

        lab.before(function (done) {

            API.setup();
            API.store.start(function (err) {
                if (err) return done(err);

                API.store.models.presentations.reset(done);
            });
        });

        lab.test('presentations list', function (done) {

            var options = {
                method: "GET",
                url: API.config.base + '/presentations'
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('presentations');

                helpers.verifyArray(result.presentations, 'id', {
                    1: helpers.validators.presentations.whatsUp,
                    2: helpers.validators.presentations.ofCourse,
                    3: helpers.validators.presentations.gettingTheMost,
                    4: helpers.validators.presentations.shaaaadUp,
                    5: helpers.validators.presentations.ahHates,
                    6: helpers.validators.presentations.theGreat
                });

                done();
            });
        });

    });
});
