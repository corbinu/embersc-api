var Lab = require('lab');

var API = require('../../../lib');
var helpers  = require('../../helpers');
var speakersController = require('../../../lib/server/controllers/speaker');

var lab = exports.lab = Lab.script();

lab.experiment('Speaker', function () {

    var id;
    var speaker;

    var validateUpdated = function (check) {
        Lab.expect(check).to.have.property('name', 'Wascally Wabbit');
        Lab.expect(check).to.have.property('presentations').to.be.an('array').length(2).to.include.members([ '1', '2' ]);
    };

    lab.before(function (done) {

        API.setup();
        API.store.start(function (err) {
            if (err) return done(err);

            API.store.models.speakers.reset(function(err) {
                if (err) return done(err);

                API.store.models.speakers.all(function(err, results) {
                    if (err) return done(err);

                    results.forEach(function(result) {
                        if (result.id === '1') {
                            speaker = results[0].serialize();
                            id = speaker.id;
                            delete speaker.id;
                        }
                    });

                    done();
                });

            });
        });
    });

    lab.test('basic', function (done) {

        Lab.expect(speakersController).to.have.property('setup').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        speakersController.setup(API.config, API.store);

        done();
    });

    lab.experiment('PUT', function() {

        lab.test('update', function (done) {
            speaker.name = 'Wascally Wabbit';

            var options = {
                method: "PUT",
                url: API.config.base + '/speakers/' + id,
                payload: { speaker: speaker }
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('speaker');
                validateUpdated(result.speaker);

                done();
            });
        });
    });

    lab.experiment('GET', function() {

        lab.test('get by ID', function (done) {

            var options = {
                method: "GET",
                url: API.config.base + '/speakers/' + id
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('speaker');
                validateUpdated(result.speaker);

                done();
            });

        });


        lab.test('get nonexistant', function (done) {

            var options = {
                method: "GET",
                url: API.config.base + '/speakers/doesnt-exist'
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(404);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 404);
                Lab.expect(result).to.have.property('error', 'Not Found');
                Lab.expect(result).to.have.property('message', 'No speaker found with the id: doesnt-exist');

                done();
            });

        });

    });

    lab.experiment('DELETE', function() {

        lab.test('remove', function (done) {
            var options = {
                method: "DELETE",
                url: API.config.base + '/speakers/' + id
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.equal(true);

                API.store.models.speakers.all(function(err, speakers) {
                    if (err) return done(err);

                    helpers.verifyArray(speakers, 'id', {
                        2: helpers.validators.speakers.wile,
                        3: helpers.validators.speakers.yosemite
                    });

                    done();
                });
            });

        });

    });
    
});
