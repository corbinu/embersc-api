var Lab = require('lab');

var API = require('../../../lib');
var resetController = require('../../../lib/server/controllers/reset');

var lab = exports.lab = Lab.script();

lab.experiment('Reset', function () {

    lab.test('basic', function (done) {

        Lab.expect(resetController).to.have.property('setup').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        resetController.setup(API.config, API.store);

        done();
    });

    lab.experiment('GET', function() {

        lab.before(function (done) {

            API.setup();
            API.store.start(done);
        });

        lab.test('get', function (done) {

            var options = {
                method: "GET",
                url: API.config.base + '/reset'
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('reset', true);

                done();
            });

        });

    });
});
