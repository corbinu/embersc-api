var Lab = require('lab');

var API = require('../../../lib');
var helpers  = require('../../helpers');
var presentationController = require('../../../lib/server/controllers/presentation');

var lab = exports.lab = Lab.script();

lab.experiment('Presentation', function () {

    var id;
    var presentation;

    var validateUpdated = function (check) {
        Lab.expect(check).to.have.property('id', '1');
        Lab.expect(check).to.have.property('title', 'Where is that turn in Albuquerque?');
        Lab.expect(check).to.have.property('speaker', '1');
    };

    lab.before(function (done) {

        API.setup();
        API.store.start(function (err) {
            if (err) return done(err);

            API.store.models.presentations.reset(function(err) {
                if (err) return done(err);

                API.store.models.presentations.all(function(err, results) {
                    if (err) return done(err);

                    results.forEach(function(result) {
                        if (result.id === '1') {
                            presentation = results[0].serialize();
                            id = presentation.id;
                            delete presentation.id;
                        }
                    });

                    done();
                });

            });
        });
    });

    lab.test('basic', function (done) {

        Lab.expect(presentationController).to.have.property('setup').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        presentationController.setup(API.config, API.store);

        done();
    });

    lab.experiment('PUT', function() {

        lab.test('update', function (done) {
            presentation.title = "Where is that turn in Albuquerque?";

            var options = {
                method: "PUT",
                url: API.config.base + '/presentations/' + id,
                payload: { presentation: presentation }
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('presentation');
                validateUpdated(result.presentation);

                done();
            });
        });
    });

    lab.experiment('GET', function() {

        lab.test('get by ID', function (done) {

            var options = {
                method: "GET",
                url: API.config.base + '/presentations/' + id
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('presentation');
                validateUpdated(result.presentation);

                done();
            });

        });

        lab.test('get nonexistant', function (done) {

            var options = {
                method: "GET",
                url: API.config.base + '/presentations/doesnt-exist'
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(404);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 404);
                Lab.expect(result).to.have.property('error', 'Not Found');
                Lab.expect(result).to.have.property('message', 'No presentation found with the id: doesnt-exist');

                done();
            });

        });

    });

    lab.experiment('DELETE', function() {

        lab.test('remove', function (done) {
            var options = {
                method: "DELETE",
                url: API.config.base + '/presentations/' + id
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.equal(true);

                API.store.models.presentations.all(function(err, presentations) {
                    if (err) return done(err);

                    helpers.verifyArray(presentations, 'id', {
                        2: helpers.validators.presentations.ofCourse,
                        3: helpers.validators.presentations.gettingTheMost,
                        4: helpers.validators.presentations.shaaaadUp,
                        5: helpers.validators.presentations.ahHates,
                        6: helpers.validators.presentations.theGreat
                    });

                    done();
                });
            });

        });

    });
});
