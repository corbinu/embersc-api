var Lab = require('lab');

var API = require('../../../lib');
var helpers  = require('../../helpers');
var speakersController = require('../../../lib/server/controllers/speakers');

var lab = exports.lab = Lab.script();

lab.experiment('Speakers', function () {

    lab.test('basic', function (done) {

        Lab.expect(speakersController).to.have.property('setup').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        speakersController.setup(API.config, API.store);

        done();
    });

    lab.experiment('POST', function() {

        lab.before(function (done) {

            API.setup();
            API.store.start(function (err) {
                if (err) return done(err);

                API.store.models.speakers.removeAll(done);
            });
        });

        lab.test('add', function (done) {
            var options = {
                method: "POST",
                url: API.config.base + '/speakers',
                payload: { speaker: helpers.data.speakers[0] }
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('speaker');
                helpers.validators.speakers.bugs(result.speaker);

                done();
            });
        });

        lab.test('add multiple', function (done) {
            var speakers = helpers.data.speakers.slice(1);

            var options = {
                method: "POST",
                url: API.config.base + '/speakers',
                payload: { speakers: speakers }
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('speakers');

                helpers.verifyArray(speakers, 'id', {
                    2: helpers.validators.speakers.wile,
                    3: helpers.validators.speakers.yosemite
                });

                done();
            });
        });

        lab.test('add again', function (done) {

            var options = {
                method: "POST",
                url: API.config.base + '/speakers',
                payload: { speakers: helpers.data.speakers[0] }
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(409);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 409);
                Lab.expect(result).to.have.property('error', 'Conflict');
                Lab.expect(result).to.have.property('message', 'Error: Speaker with id 1 already exists');

                done();
            });
        });
    });

    lab.experiment('GET', function() {

        lab.before(function (done) {

            API.setup();
            API.store.start(function (err) {
                if (err) return done(err);

                API.store.models.speakers.reset(done);
            });
        });

        lab.test('speakers list', function (done) {

            var options = {
                method: "GET",
                url: API.config.base + '/speakers'
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('speakers');

                helpers.verifyArray(result.speakers, 'id', {
                    1: helpers.validators.speakers.bugs,
                    2: helpers.validators.speakers.wile,
                    3: helpers.validators.speakers.yosemite
                });

                done();
            });
        });

    });
});
