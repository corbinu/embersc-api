var models = {};

models.speakers =  require('./speakers');
models.presentations =  require('./presentations');

models.setup = function(config, db) {
    models.speakers.setup(config, db);
    models.presentations.setup(config, db);
};

models.start = function(callback) {
    models.speakers.start(function (err) {
        if (err) return callback(err);

        models.presentations.start(callback);
    });
};

module.exports = models;
