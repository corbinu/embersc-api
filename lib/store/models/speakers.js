var MRStore = require('mrstore');

var install = [
    {
        id: '1',
        name: 'Bugs Bunny',
        presentations: [ '1', '2' ]
    },
    {
        id: '2',
        name: 'Wile E. Coyote',
        presentations: [ '3' ]
    },
    {
        id: '3',
        name: 'Yosemite Sam',
        presentations: [ '4', '5', '6' ]
    }
];

var Speaker = new MRStore('Speaker', {
    name: String,
    presentations: {
        _type: Array,
        _default: []
    }
});

Speaker.reset = function (callback) {
    Speaker.removeAll(function (err) {
        if (err) return callback(err);

        Speaker.insert(install, callback);
    });
};

Speaker.setup = function(config, db) {
    Speaker.internal.config = config;
    Speaker.internal.db = db;
};

Speaker.start = function(callback) {
    Speaker.connect(Speaker.internal.db.mrstore.speakers, function(err) {
        if (err) return callback(err);

        Speaker.installViews(callback);
    });
};

module.exports = Speaker;