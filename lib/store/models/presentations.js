var MRStore = require('mrstore');

var install = [
    {
        id: '1',
        title: "What's up with Docs?",
        speaker: "1"
    },
    {
        id: '2',
        title: "Of course, you know, this means war.",
        speaker: "1"
    },
    {
        id: '3',
        title: "Getting the most from the Acme catalog.",
        speaker: "2"
    },
    {
        id: '4',
        title: "Shaaaad up!",
        speaker: "3"
    },
    {
        id: '5',
        title: "Ah hates rabbits.",
        speaker: "3"
    },
    {
        id: '6',
        title: "The Great horni-todes",
        speaker: "3"
    }
];

var Presentation = new MRStore('Presentation', {
    title: String,
    speaker: String
});

Presentation.reset = function (callback) {
    Presentation.removeAll(function (err) {
        if (err) return callback(err);

        Presentation.insert(install, callback);
    });
};

Presentation.setup = function(config, db) {
    Presentation.internal.config = config;
    Presentation.internal.db = db;
};

Presentation.start = function(callback) {
    Presentation.connect(Presentation.internal.db.mrstore.presentations, function(err) {
        if (err) return callback(err);

        Presentation.installViews(callback);
    });
};

module.exports = Presentation;