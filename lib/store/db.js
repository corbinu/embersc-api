var Datastore = require('nedb');

var db = {
    mrstore: {}
};

db.setup = function(config) {

    switch (config.mrstore.type) {
        case "mock":
            db.mrstore.speakers = {
                type: 'nedb',
                datastore: new Datastore()
            };
            db.mrstore.presentations = {
                type: 'nedb',
                datastore: new Datastore()
            };
            break;
        case "nedb":
            db.mrstore.speakers = {
                type: "nedb",
                datastore: new Datastore({ filename: 'db/users.db', autoload: true })
            };
            db.mrstore.presentations = {
                type: "nedb",
                datastore: new Datastore({ filename: 'db/presentations.db', autoload: true })
            };
            break;
        default:
            throw new Error("Invalid connection type: " + connection.type);
    }

};

module.exports = db;
