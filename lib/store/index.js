var store = {};

store.db = require('./db');
store.models = require('./models');

store.setup = function(config) {
	store.db.setup(config);
	store.models.setup(config, store.db);
};

store.start = function(callback) {
    store.models.start(callback);
};

module.exports = store;
