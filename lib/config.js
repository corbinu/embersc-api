var nconf = require('nconf');

var config = {};

nconf.env().argv();

if (nconf.get('NODE_ENV') === 'production') {
	config = require('../config/production.json');
	config.env = 'production';
} else if (nconf.get('NODE_ENV') === 'stage') {
	config = require('../config/stage.json');
	config.env = 'stage';
} else if (nconf.get('NODE_ENV') === 'test') {
	config = require('../config/test.json');
	config.env = 'test';
} else {
	config = require('../config/dev.json');
	config.env = 'dev';
}

config.nconf = nconf;

module.exports = config;