var API = {};

API.config = require('./config');
API.store = require('./store');
API.server = require('./server');

API.setup = function() {
    API.store.setup(API.config);
    API.server.setup(API.config, API.store);
};

API.start = function(callback) {
    API.store.start(function(err) {
        if (err) {
            throw err;
        }
        API.server.start(callback);
    });
};

module.exports = API;
