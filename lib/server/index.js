var Hapi = require('hapi');

var server = {};

server.controllers = require('./controllers');

server.setup = function(config, store) {
	server.controllers.setup(config, store);

	server.instance = new Hapi.Server(config.hostname, config.port, {
		cors: true
	});

	server.instance.pack.register([
			{
				plugin: require('good'),
				options: config.good
			},
			{
				plugin: require('lout'),
				options: config.lout
			}
		],
		function () {}
	);

	server.instance.route([
		{
			method: 'GET',
			path: '/',
			config: {
				handler: function(request, reply) {
					reply.redirect(config.base + '/');
				}
			}
		},
		{
			method: 'GET',
			path: config.base + '/',
			config: server.controllers.base.config
		},
        {
            method: 'GET',
            path: config.base + '/reset',
            config: server.controllers.reset.config
        },
        {
            method: [ 'GET', 'POST' ],
            path: config.base + '/speakers',
            config: server.controllers.speakers.config
        },
        {
            method: [ 'GET', 'PUT', 'DELETE' ],
            path: config.base + '/speakers/{id}',
            config: server.controllers.speaker.config
        },
        {
            method: [ 'GET', 'POST' ],
            path: config.base + '/presentations',
            config: server.controllers.presentations.config
        },
        {
            method: [ 'GET', 'PUT', 'DELETE' ],
            path: config.base + '/presentations/{id}',
            config: server.controllers.presentation.config
        }
	]);
};

server.start = function(callback) {

    server.instance.start(function () {
        callback(server.instance.info);
    });

};

module.exports = server;
