var baseController = {
	config: {}
};

baseController.setup = function(config, store) {

	baseController.config.handler = function (request, reply) {
		reply({
			'hello': 'Welcome to the ' + config.name +  ' API',
			'version': 'v1'
		});
	};

};

module.exports = baseController;