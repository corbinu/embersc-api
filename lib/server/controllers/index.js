var controllers = {};

controllers.base = require('./base');
controllers.reset = require('./reset');
controllers.speakers = require('./speakers');
controllers.speaker = require('./speaker');
controllers.presentations = require('./presentations');
controllers.presentation = require('./presentation');

controllers.setup = function(config, store) {
	controllers.base.setup(config, store);
    controllers.reset.setup(config, store);
    controllers.speakers.setup(config, store);
    controllers.speaker.setup(config, store);
    controllers.presentations.setup(config, store);
    controllers.presentation.setup(config, store);
};

module.exports = controllers;
