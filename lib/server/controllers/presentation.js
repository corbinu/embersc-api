var Hapi = require('hapi');

var presentationController = {
    config: {}
};

presentationController.setup = function(config, store) {

    presentationController.config.handler = function (request, reply) {
        var id = request.params.id;

        if (request.method === 'get') {

            store.models.presentations.get(id, function (err, presentation) {
                if (err) return reply(Hapi.error.internal('DB get error', err));
                if ( ! presentation) return reply(Hapi.error.notFound('No presentation found with the id: ' + id));

                reply({ presentation: presentation.serialize() });
            });

        } else if (request.method === 'delete') {

            store.models.presentations.remove(id, function(err) {
                if (err) return reply(Hapi.error.internal('DB remove error', err));

                return reply(true);
            });

        } else if (request.method === 'put') {
            var doc = request.payload.presentation;
            doc.id = request.params.id;

            store.models.presentations.upsert(doc, function(err, presentation) {
                if (err) return reply(Hapi.error.internal('DB update error', err));

                return reply({ presentation: presentation.serialize() });
            });

        }
    };

};

module.exports = presentationController;