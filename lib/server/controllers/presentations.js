var Hapi = require('hapi');

var presentationsController = {
    config: {}
};

presentationsController.setup = function(config, store) {

    presentationsController.config.handler = function (request, reply) {
        if (request.method === 'post') {
            var doc = request.payload.presentation;

            if (doc) {
                store.models.presentations.insert(doc, function(err, presentation) {
                    if (err) {
                        if (err.message.indexOf("already exists") > 0) {
                            var error = Hapi.error.badRequest(err);
                            error.output.statusCode = 409;
                            error.reformat();

                            return reply(error);
                        }
                        return reply(Hapi.error.internal('DB insert error', err));
                    }

                    return reply({ presentation: presentation.serialize() });
                });

            } else {
                var docs = request.payload.presentations;

                store.models.presentations.insert(docs, function(err, presentations) {
                    if (err) {
                        if (err.message.indexOf("already exists") > 0) {
                            var error = Hapi.error.badRequest(err);
                            error.output.statusCode = 409;
                            error.reformat();

                            return reply(error);
                        }
                        return reply(Hapi.error.internal('DB insert error', err));
                    }

                    presentations = presentations.map(function (presentation) {
                        return presentation.serialize();
                    });

                    return reply({ presentations: presentations });
                });
            }

        } else if (request.method === 'get') {

            store.models.presentations.all(function (err, presentations) {
                if (err) return reply(Hapi.error.internal('DB insert error', err));

                presentations = presentations.map(function (presentation) {
                    return presentation.serialize();
                });

                reply({ presentations: presentations });
            });

        }
    };

};

module.exports = presentationsController;