var Hapi = require('hapi');

var speakersController = {
    config: {}
};

speakersController.setup = function(config, store) {

    speakersController.config.handler = function (request, reply) {

        if (request.method === 'post') {
            var doc = request.payload.speaker;

            if (doc) {
                store.models.speakers.insert(doc, function(err, speaker) {
                    if (err) {
                        if (err.message.indexOf("already exists") > 0) {
                            var error = Hapi.error.badRequest(err);
                            error.output.statusCode = 409;
                            error.reformat();

                            return reply(error);
                        }
                        return reply(Hapi.error.internal('DB insert error', err));
                    }

                    return reply({ speaker: speaker.serialize() });
                });

            } else {
                var docs = request.payload.speakers;

                store.models.speakers.insert(docs, function(err, speakers) {
                    if (err) {
                        if (err.message.indexOf("already exists") > 0) {
                            var error = Hapi.error.badRequest(err);
                            error.output.statusCode = 409;
                            error.reformat();

                            return reply(error);
                        }
                        return reply(Hapi.error.internal('DB insert error', err));
                    }

                    speakers = speakers.map(function (speaker) {
                        return speaker.serialize();
                    });

                    return reply({ speakers: speakers });
                });
            }

        } else if (request.method === 'get') {

            store.models.speakers.all(function (err, speakers) {
                if (err) return reply(Hapi.error.internal('DB insert error', err));

                speakers = speakers.map(function (speaker) {
                    return speaker.serialize();
                });

                reply({ speakers: speakers });
            });

        }
    };

};

module.exports = speakersController;