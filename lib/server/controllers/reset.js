var Hapi = require('hapi');

var resetController = {
    config: {}
};

resetController.setup = function(config, store) {

    resetController.config.handler = function (request, reply) {

        if (config.env === 'test' || config.env === 'dev') {

            store.models.speakers.reset(function (err) {
                if (err) return reply(Hapi.error.internal('DB reset error', err));

                store.models.presentations.reset(function (err) {
                    if (err) return reply(Hapi.error.internal('DB reset error', err));

                    reply({
                        'reset': true
                    });
                });
            });
        } else {
            reply(Hapi.error.forbidden('Only allowed in development and testing'));
        }
    };

};

module.exports = resetController;