var Hapi = require('hapi');

var speakerController = {
    config: {}
};

speakerController.setup = function(config, store) {

    speakerController.config.handler = function (request, reply) {
        var id = request.params.id;

        if (request.method === 'get') {

            store.models.speakers.get(id, function (err, speaker) {
                if (err) return reply(Hapi.error.internal('DB get error', err));
                if ( ! speaker) return reply(Hapi.error.notFound('No speaker found with the id: ' + id));

                reply({ speaker: speaker.serialize() });
            });

        } else if (request.method === 'delete') {

            store.models.speakers.remove(id, function(err) {
                if (err) return reply(Hapi.error.internal('DB remove error', err));

                return reply(true);
            });

        } else if (request.method === 'put') {
            var doc = request.payload.speaker;
            doc.id = request.params.id;

            store.models.speakers.upsert(doc, function(err, speaker) {
                if (err) return reply(Hapi.error.internal('DB update error', err));

                return reply({ speaker: speaker.serialize() });
            });

        }
    };

};

module.exports = speakerController;